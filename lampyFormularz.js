$(document).ready(function() {

    //suwak, tworzenie pól
    $(".suwak").val(1);
    $(".newFieldz").val("");
    $("#przelaczniki").hide();
    $("#trad").hide();
    $("#led").hide();
    $('input[class="pickbulb"]').prop('checked', false);
    $('input[type="checkbox"]').prop('checked', false);
    $("#d_all .newFieldz").hide();
    $("#k_all label.pickcolour").hide();

    $(".suwak").change(function() {
        $("#suwak1lab").text($("#suwak1").val())
    })
    $(".suwak").mousemove(function() {
        $("#suwak1lab").text($("#suwak1").val())
    })

    /* grafika zarowki
    $(".lamps").hide()
    $(".lamps").slideDown("slow");
    $(".rounds").animate({ top: '290' }, "slow");
    $(".rounds2").animate({ top: '300' }, "slow");
    */

    $(function CreateInputs() {
        var newFields = $('');

        $('#suwak1').bind('blur keyup change', function() {
            var n = $("#suwak1").val();
            if (n) {
                if (n > newFields.length) {
                    addFields(n);
                    setTimeout(ToggleSwitch, 1000);
                } else {
                    removeFields(n);
                    setTimeout(ToggleSwitch, 1000);
                }
            }

            function ToggleSwitch() {
                if (n > 1) {
                    $("#przelaczniki").slideDown("slow");
                } else {
                    $("#przelaczniki").slideUp("slow");
                }
            }

        });

        function addFields(n) {
            for (i = newFields.length; i < n - 1; i++) {
                var fieldnumber = i + 2;
                var input = $('<div class="container2"><a>Kabel nr ' + fieldnumber + '</a><input class="newFieldz" type="text" name="length[] " placeholder="wprowadź długość kabla ' + fieldnumber + '..." /> \
                            <input checked type="radio" class="pickcolour" name="pickcolour' + fieldnumber + '" id="black' + fieldnumber + '" value="czarny" /> \
                            <label class="pickcolour" for="black' + fieldnumber + '" name="lblack"></label> \
                            <input type="radio" class="pickcolour" name="pickcolour' + fieldnumber + '" id="red' + fieldnumber + '" value="czerwony" /> \
                            <label class="pickcolour" for="red' + fieldnumber + '" name="lred"></label> \
                            <input type="radio" class="pickcolour" name="pickcolour' + fieldnumber + '" id="blue' + fieldnumber + '" value="niebieski" /> \
                            <label class="pickcolour" for="blue' + fieldnumber + '" name="lblue"></label> \
                            <input type="radio" class="pickcolour" name="pickcolour' + fieldnumber + '" id="green' + fieldnumber + '" value="zielony" /> \
                            <label class="pickcolour" for="green' + fieldnumber + '" name="lgreen"></label> </div>');
                var newInput = input.clone();
                newFields = newFields.add(newInput);
                newInput.appendTo('#newFields').hide(0).fadeIn(1000);
            }
        }

        function removeFields(n) {
            var removeField = newFields.slice(n - 1).remove();
            newFields = newFields.not(removeField);
        }
    });

    //reszta
    $(".total2").text("Do zapłaty: 0.00zł");
    $('input[name="bulb"]').on('change', ToggleSwitch2);
    $('input[type="checkbox"]').on('change', ToggleSwitch3);



    function ToggleSwitch3() {

        if ($('#dlugosc').is(":checked")) {
            $("#newFields .newFieldz").hide();
            $("#d_all .newFieldz").show();
            if ($('#kolor').is(":checked")) { $("#newFields").hide(); }
        } else {
            $("#newFields .newFieldz").show();
            $("#d_all .newFieldz").hide();
            $("#newFields").show();
        }

        if ($('#kolor').is(":checked")) {
            $("#newFields label.pickcolour").hide();
            $("#k_all label.pickcolour").show();
            if ($('#dlugosc').is(":checked")) { $("#newFields").hide(); }
        } else {
            $("#newFields label.pickcolour").show();
            $("#k_all label.pickcolour").hide();
            $("#newFields").show();
        }

    }

    function ToggleSwitch2() {

        if ($('#bulb1').is(':checked')) {
            $("#led").slideDown("slow");
            $("#trad").slideUp("slow");
            $('input[name="moctrad"]').prop('checked', false);
        }

        if ($('#bulb2').is(':checked')) {
            $("#trad").slideDown("slow");
            $("#led").slideUp("slow");
            $('input[name="mocled"]').prop('checked', false);
        }

    }

    $(document).on("change", 'input,select', function TotalSum() {

        var value = $('input[type=text]').val();
        value = value.replace(/\D/g, "");

        var cenabulb = 0;
        var ilosckabli = Number($('.suwak').val());
        var cenaled = $('input[name="mocled"]:checked').val();
        var cenatrad = $('input[name="moctrad"]:checked').val();
        if (isNaN(cenatrad)) {
            cenatrad = 0;
        }
        if (isNaN(cenaled)) {
            cenaled = 0;
        }
        cenaled = Number(cenaled);
        cenatrad = Number(cenatrad);
        cenabulb = (cenaled + cenatrad) * ilosckabli;

        var sum = 0;
        if ($('#dlugosc').is(":checked")) {

            $('#d_all .newFieldz').each(function CableSum() {
                var elem = this.value;
                elem = Number(elem) * Number($('.suwak').val());
                sum += elem;
            });

        } else {
            $('#newFields .newFieldz').each(function CableSum() {
                var elem = this.value;
                elem = Number(elem);
                sum += elem;
            });
        }
        sum = sum / 100 * 32;

        var total = sum + cenabulb;
        $("#hidden").val(total.toFixed(2));
        $(".total2").text("Do zapłaty: " + total.toFixed(2) + "zł");
    });
});